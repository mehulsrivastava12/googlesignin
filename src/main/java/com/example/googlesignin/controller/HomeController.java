package com.example.googlesignin.controller;

import com.example.googlesignin.entity.User;
import com.example.googlesignin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @Autowired
    UserService userService;

    @RequestMapping("/")
    public String login() {
        return "login";
    }

    @GetMapping("/oauth2/code/google")
    public String home(Model model, @AuthenticationPrincipal OAuth2User OAuth2User) {
        String name = OAuth2User.getAttribute("name");
        String email = OAuth2User.getAttribute("email");
        User user = userService.findUserByEmail(email);
        if(user == null){
            userService.save(email,name);
        }
        model.addAttribute("name", name);
        model.addAttribute("email", email);
        return "index";
    }
}
