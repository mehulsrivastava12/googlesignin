package com.example.googlesignin.service;

import com.example.googlesignin.entity.User;
import com.example.googlesignin.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User findUserByEmail(String email){
        User user = userRepository.findByEmail(email);
        return user;
    }

    public void save(String email, String name){
        User user = new User();
        user.setEmail(email);
        user.setName(name);
        userRepository.save(user);
    }
}
