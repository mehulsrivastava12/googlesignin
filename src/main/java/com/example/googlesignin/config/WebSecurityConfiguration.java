package com.example.googlesignin.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

//@Configuration
//@EnableWebSecurity
//public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .csrf().disable()
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                .and()
//                .authorizeRequests()
////                .antMatchers("/").permitAll()
//                .anyRequest().authenticated() // Require authentication for other endpoints
//                .and()
//                .oauth2Login()
////                .defaultSuccessUrl("/oauth2/code/google")
////                .loginPage("/login")
//                .permitAll()
//                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//    }
//}
@Configuration
public class WebSecurityConfiguration {


    @Bean
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(authorizeRequests ->
                        authorizeRequests
                                .antMatchers("/").permitAll() // Allow access to the home page without authentication
                                .anyRequest().authenticated()
                )
                .oauth2Login(oauth2Login ->
                        oauth2Login
                                .loginPage("/login") // Specify the custom login page
                                .defaultSuccessUrl("/oauth2/code/google", true) // Specify the custom success URL
                )
                .logout(logout -> logout
                        .logoutSuccessUrl("/") // Specify the logout success URL
                        .clearAuthentication(true) // Clear the authentication after logout
                );
        return http.build();
    }
}
